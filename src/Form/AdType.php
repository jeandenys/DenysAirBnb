<?php

namespace App\Form;

use App\Entity\Ad;
use App\Form\ImageType;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class AdType extends ApplicationType
{
       
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, $this->getConfiguration("Titre","Saisie un titre mettant en valeur votre appartement"))
            ->add('slug', TextType::class, $this->getConfiguration("Tapez l'adresse web","l'adresse web automatique", ["required"=>false]))
            ->add('introduction', TextType::class, $this->getConfiguration("Introduction","Décrivez brièvement votre location"))
            ->add('content', TextareaType::class, $this->getConfiguration("Description", "indiquez une presentation qui donne envie"))
            ->add('coverImage', UrlType::class, $this->getConfiguration("Image principale ", "indiquez une url d'image qui donne envie"))
            ->add('price', MoneyType::class, $this->getConfiguration("Prix", "Saisie du prix pour une nuit"))
            ->add('rooms', IntegerType::class, $this->getConfiguration("Nombre de chambre", "indiquez ici le nombre de chambre") )
            ->add(
                'images',
                CollectionType::class, 
                [
                    'entry_type' => ImageType::class,
                    'allow_add' => true,
                    'allow_delete'=> true
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
