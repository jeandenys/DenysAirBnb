<?php 
namespace App\Form;

use Symfony\Component\Form\AbstractType;

class ApplicationType extends AbstractType
{
        /**
     * Récupere le style pour les champs du formulaire
     *
     * @param [type] $label
     * @param [type] $placeholder
     * @param [array] $options
     * @return void
     */
    protected function getConfiguration($label, $placeholder, $options = []) {
        return array_merge([
            'label'=> $label,
            'attr' => [
                'placeholder'=>$placeholder
            ]
            ], $options );
    }

    
}