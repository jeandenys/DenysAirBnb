<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller  {

    /*
    * @Route("/hello/{number}",  name="hello")
    */
    public function hello ($number=1963, $nom="anonyme", $prenom="anonyme")
    {

      return new Response("Salut mon ".$nom." ".$prenom." quand es tu né en  :".$number) ;
    }


    /*
    * @Route("/", name="homepage")
    */
    public function home ()
    {$tableau = ["prenom"=>"Denis"];
      //return new Response("bienvenue maison") ;
      return $this->render('home.html.twig',["title"=>"bonjour à tous","age"=>18, 'tableau'=> $tableau]);
    }

}
?>