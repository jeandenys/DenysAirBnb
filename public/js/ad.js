

    $('#add-image').click(function(){
        //const index = $('#ad_images div.form-group').length;
        const index = + $('#widgets-counter').val();
        //recup prototype des entrées
        const tmpl  = $('#ad_images').data('prototype').replace(/__name__/g, index);
        // injection de code dans la div
        $('#ad_images').append(tmpl);
        $('#widgets-counter').val(index + 1)

        handleDeleteButtons();

    });
    // gestion du bouton supprimer
    function  handleDeleteButtons() {
        $('button[data-action="delete"]').click(function(){
            const target = this.dataset.target;
            $(target).remove();
        });
    }

    function updateCounter(){
        const count = +$('#ad_images div.form-group').length;
        $('#widgets-counter').val(count)
    }
    updateCounter();
    handleDeleteButtons();

